(defproject prom-crypto "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [http-kit "2.2.0"]
                 [cheshire "5.8.1"]
                 [iapetos "0.1.8"]]
  :main ^:skip-aot prom-crypto.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
