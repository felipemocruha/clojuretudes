(ns prom-crypto.core
  (:require [org.httpkit.client :as http]
            [cheshire.core :as json]
            [iapetos.core :as prom]
            [iapetos.standalone :as standalone])
  (:gen-class))

(defonce config {:baseurl "https://api.coingecko.com/api/v3/coins"})

(defonce registry
  (-> (prom/collector-registry)
      (prom/register
       (prom/counter :btc/req-count)
       (prom/gauge :btc/current-price-usd)
       (prom/gauge :btc/current-price-eth)
       (prom/gauge :btc/market-cap-usd)
       (prom/gauge :btc/market-cap-eth)
       (prom/gauge :btc/total-volume-usd)
       (prom/gauge :btc/total-volume-eth)
       (prom/gauge :btc/last-update)

       (prom/counter :eth/req-count)
       (prom/gauge :eth/current-price-usd)
       (prom/gauge :eth/current-price-btc)
       (prom/gauge :eth/market-cap-usd)
       (prom/gauge :eth/market-cap-btc)
       (prom/gauge :eth/total-volume-usd)
       (prom/gauge :eth/total-volume-btc)
       (prom/gauge :eth/last-update))))

(defonce httpd
  (standalone/metrics-server registry {:port 8080}))

(defn make-url [config coin]
  (format "%s/%s" (:baseurl config) coin))

(defn make-request [url]
  (-> @(http/get url)
      :body
      (json/decode true)))

(defn current-price [resp unit]
  (let [u (keyword unit)]
    (-> resp :market_data :current_price u)))

(defn last-update [resp] (:last_updated resp))

(defn market-cap [resp unit]
  (let [u (keyword unit)]
    (-> resp :market_data :market_cap u)))

(defn total-volume [resp unit]
  (let [u (keyword unit)]
    (-> resp :market_data :total_volume u)))

(defn -main [& args]
  (while true
    (println "fetching bitcoin info")
    (let [btc (make-request (make-url config "bitcoin"))]
      (-> registry
          (prom/inc :btc/req-count)
          (prom/set :btc/current-price-usd (current-price btc "usd"))
          (prom/set :btc/current-price-eth (current-price btc "eth"))
          (prom/set :btc/market-cap-usd (market-cap btc "usd"))
          (prom/set :btc/market-cap-eth (market-cap btc "eth"))
          (prom/set :btc/total-volume-usd (total-volume btc "usd"))
          (prom/set :btc/total-volume-eth (total-volume btc "eth"))))

    (println "fetching ethereum info")
    (let [eth (make-request (make-url config "ethereum"))]
      (-> registry
          (prom/inc :eth/req-count)
          (prom/set :eth/current-price-usd (current-price eth "usd"))
          (prom/set :eth/current-price-btc (current-price eth "btc"))
          (prom/set :eth/market-cap-usd (market-cap eth "usd"))
          (prom/set :eth/market-cap-btc (market-cap eth "btc"))
          (prom/set :eth/total-volume-usd (total-volume eth "usd"))
          (prom/set :eth/total-volume-btc (total-volume eth "btc"))))

    (Thread/sleep 60000)))
