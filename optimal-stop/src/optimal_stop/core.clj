(ns optimal-stop.core
  (:require [clojure.spec.alpha :as s])
  (:gen-class))

(s/def ::rank-is-set #(= (count %) (count (set %))))
(s/def ::int-seq (s/every #(int? %)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
